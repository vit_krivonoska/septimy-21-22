# Septimy 21-22

GYMVOD programovací seminář Septimy 2021/2022

## Učitelé

**David Hájek**

- hajekdd@gmail.com
- FEL
- Java, php , python, javascript


### TEST

### Soutěž Kasiopea
- https://kasiopea.matfyz.cz/


### Pravidla

- Všichni se naučíme pracovat s GITem. Materiály budou na GitLabu.
- Discord https://discord.gg/n2c2rhBKyr
- Vždycky si to z něho na začátku hodiny stáhneme.
- Úkoly budou každý týden nebo ob týden. Na vypracování máte týden,
s dobrou výmluvou máte dva týdny. Jinak dostanete úkol navíc. Za
úkoly se budou dostávat jedničky. Neopisovat, poznám to. Odevzdávat
na svůj repositář.
- Budete mít projekt na semestr ve dvojicích. Témata si představíme
příští hodinu.
- Písemky budou asi dvě - část teoretická a část praktická.
- Při hodině budete spolupracovat.

### Harmonogram 

| Datum | Učitel | Téma | Přednášky | Úkol |
| --- | --- | --- | --- | --- |
| 7.9.2021 | David | Úvod; seznámení; stránka; úkoly; semestrálka; postup při řešení problému; vlastnosti algoritmu, základní struktury; basic úloha | [lec01](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.09.07/uvodni-prezentace.pdf) | |
| 14.9.2021 | David | GIT, založit repo, nasdílet, ssh klíč; pull, add ., commit, push, rebase, merge | [lec02](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.09.14/prezentace02.pdf) | |
| 21.9.2021 | David |GIT, typy programovacích jazyků | [lec03](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.09.21/prezentace03.pdf) | [hw01](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw01) |
| 28.9.2021 |  | svátek |  |  |
| 5.10.2021 | David |datové typy hodnotové a referenční; předdefinované datové typy; převody mezi datovými typy a jejich využití; deklarace proměnných | [lec04](https://gitlab.com/hajekdd/septimy-21-22/-/blob/main/lectures/2021.10.05/prezentace04.pdf) | [hw02](https://gitlab.com/hajekdd/septimy-21-22/-/tree/main/homework/hw02) |


